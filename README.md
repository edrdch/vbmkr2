Here are some instruction, files and scripts to install Void Linux on a Macbook via chroot.
This is NOT the easy way of doing. 
If you don't understand what's written in this README file, don't go further.
Take some time to practice installing Arch Linux in a virtual box, learn BASH and then, come back.
If you know what you're doing, this will be fun.
Most of the first part is taken from the [Void Wiki](https://wiki.voidlinux.org/Main_Page).
Then, I wrote few scripts to ease the process and have a nice dwm/dmenu/st config.

Unless you are allready running Void, you'll need to download and compile [XBPS](https://github.com/void-linux/xbps).

STEP 1:

With your prefered partitioner, edit your hard drive.
In this exemple we are going to install Void in dual boot on a drive that allready contains OSX so /dev/sdb1 and /dev/sdb2 allready exist but if you start from scratch, check the wiki.

    /dev/sdb1 /boot/efi vfat  boot  
    /dev/sdb2 APPLE     hfs+  
    /dev/sdb3 /boot     ext2  
    /dev/sdb4 SWAP      swap  
    /dev/sdb5 /         ext4  


STEP 2:

If you start on an empty Drive or if you wish to create more partitions for /temp or /var or /home you'll, obviously, have to change the mount points but for simplicity's sake we'll stay on that scheme.

    # mount /dev/sdb5 /mnt  
    # mkdir /mnt/boot  
    # mount /dev/sdb3 /mnt/boot
    # mkdir /mnt/boot/efi
    # mount /dev/sdb1 /mnt/boot/efi

Now we can install the base system. 
Note that Void can also come with musl as the default libC but for a desktop computer it's probably better to go with the standard glibC for compatibility with some bloated software. (If you are installing Void on a Raspberry Pi on the other hand, the musl flavor works just fine). 

    # xbps-install -S -R http://alpha.de.repo.voidlinux.org/current -r /mnt base-system grub-x86_64-efi


Now we prepare the chroot jail

    # mount -t proc proc /mnt/proc  
    # mount -t sysfs sys /mnt/sys  
    # mount -o bind /dev /mnt/dev  
    # mount -t devpts pts /mnt/dev/pts  

Add a resolc.conf tuned for OpenNic

    # cp -L path/to/this/repo/files/etc/resolv.conf /mnt/etc/

Or add your own

    # cp -L /etc/resolv.conf /mnt/etc/